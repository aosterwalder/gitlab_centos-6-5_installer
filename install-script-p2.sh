#!/bin/bash
# Installer for GitLab on CentOS 6.5
# 
# based on installer script
# https://github.com/mattias-ohlsson/gitlab-installer/blob/master/gitlab-install-el6.sh)
# by mattias.ohlsson@inprose.com
# and gitlab-recipes https://github.com/gitlabhq/gitlab-recipes/tree/master/install/centos
#
# Only run this on a clean machine (centos minimal install). I take no responsibility for anything.
#
# Submit issues here: https://bitbucket.org/aosterwalder/gitlab_centos-6-5_installer

# Define the public hostname
export GL_HOSTNAME=$HOSTNAME

cd /home/git/gitlab

# Install Init Script
# Download the init script (will be /etc/init.d/gitlab):
wget -O /etc/init.d/gitlab https://gitlab.com/gitlab-org/gitlab-recipes/raw/master/init/sysvinit/centos/gitlab-unicorn
chmod +x /etc/init.d/gitlab
chkconfig --add gitlab

# Make GitLab start on boot:
chkconfig gitlab on

# Set up logrotate
sudo cp lib/support/logrotate/gitlab /etc/logrotate.d/gitlab

# Check if GitLab and its environment are configured correctly:
sudo -u git -H bundle exec rake gitlab:env:info RAILS_ENV=production

# Start your GitLab instance:
service gitlab start

# Compile assets
sudo -u git -H bundle exec rake assets:precompile RAILS_ENV=production

# Nginx
yum -y install nginx
chkconfig nginx on
wget -O /etc/nginx/conf.d/gitlab.conf https://gitlab.com/gitlab-org/gitlab-recipes/raw/master/web-server/nginx/gitlab-ssl

# Edit /etc/nginx/conf.d/gitlab and replace git.example.com with your FQDN. Make sure to read the comments in order to properly set up ssl.
sed -i "s/git.example.com/$GL_HOSTNAME/g" /etc/nginx/conf.d/gitlab.conf

# Add nginx user to git group:
usermod -a -G git nginx
chmod g+rx /home/git/

# generate self-signed cert
cd /etc/nginx/
#sudo openssl req -new -x509 -nodes -days 3560 -out gitlab.crt -keyout gitlab.key
#sudo chmod o-r gitlab.key

# Configure the firewall
## Poke an iptables hole so users can access the web server (http and https ports) and ssh.
lokkit -s http -s https -s ssh

# Restart the service for the changes to take effect:
service iptables save
service iptables restart

# Finally start nginx with:
service nginx start


### Enable and start

service gitlab start

echo "### Done ###############################################"
echo "#"
echo "# You have your PostgreSQL git password in this file:"
echo "# /home/git/gitlab/config/database.pwd"
echo "#"
echo "# Point your browser to:"
echo "# https://$GL_HOSTNAME (or: https://<host-ip>)"
echo "# Default admin username: admin@local.host"
echo "# Default admin password: 5iveL!fe"
echo "#"
echo "###"