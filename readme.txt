mkdir /etc/nginx/
cd /etc/nginx/
sudo openssl req -new -x509 -nodes -days 3560 -out gitlab.crt -keyout gitlab.key
sudo chmod o-r gitlab.key

curl https://bitbucket.org/aosterwalder/gitlab_centos-6-5_installer/raw/master/install-script-p1.sh | bash

cd /home/git/gitlab
sudo -u git -H bundle exec rake gitlab:setup RAILS_ENV=production

curl https://bitbucket.org/aosterwalder/gitlab_centos-6-5_installer/raw/master/install-script-p2.sh | bash

