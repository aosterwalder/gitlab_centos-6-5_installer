#!/bin/bash
# Installer for GitLab on CentOS 6.5
# 
# based on installer script
# https://github.com/mattias-ohlsson/gitlab-installer/blob/master/gitlab-install-el6.sh)
# by mattias.ohlsson@inprose.com
# and gitlab-recipes https://github.com/gitlabhq/gitlab-recipes/tree/master/install/centos
#
# Only run this on a clean machine (centos minimal install). I take no responsibility for anything.
#
# Submit issues here: https://bitbucket.org/aosterwalder/gitlab_centos-6-5_installer

# Define the public hostname
export GL_HOSTNAME=$HOSTNAME

# Define MySQL root password
POSTGRESQL_GIT_PW=$(cat /dev/urandom | tr -cd [:alnum:] | head -c ${1:-16})

# Exit on error

die()
{
  # $1 - the exit code
  # $2 $... - the message string

  retcode=$1
  shift
printf >&2 "%s\n" "$@"
  exit $retcode
}

echo "### Check OS (we check if the kernel release contains el6)"
uname -r | grep "el6" || die 1 "Not RHEL or CentOS 6 (el6)"

# Install linux standard tools
yum -y update
yum -y install wget
yum -y install vim-enhanced
yum -y install man

# Add EPEL repository
## EPEL is a volunteer-based community effort from the Fedora project to create a repository of high-quality add-on packages that complement the Fedora-based Red Hat Enterprise Linux (RHEL) and its compatible spinoffs, such as CentOS and Scientific Linux.
## As part of the Fedora packaging community, EPEL packages are 100% free/libre open source software (FLOSS).
## Download the GPG key for EPEL repository from fedoraproject and install it on your system:
wget -O /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6 https://www.fedoraproject.org/static/0608B895.txt
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6

## Optional for check: rpm -qa gpg*
## --> result should contains that: gpg-pubkey-0608b895-4bd22942

rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

# Add PUIAS Computational repository
## Create /etc/yum.repos.d/PUIAS_6_computational.repo and add the following lines
cat > /etc/yum.repos.d/PUIAS_6_computational.repo << EOF
[PUIAS_6_computational]
name=PUIAS computational Base $releasever - \$basearch
mirrorlist=http://puias.math.ias.edu/data/puias/computational/\$releasever/\$basearch/mirrorlist
#baseurl=http://puias.math.ias.edu/data/puias/computational/\$releasever/\$basearch
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-puias
EOF

## Next download and install the gpg key.
wget -O /etc/pki/rpm-gpg/RPM-GPG-KEY-puias http://springdale.math.ias.edu/data/puias/6/x86_64/os/RPM-GPG-KEY-puias
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-puias

## Optional for check: rpm -qa gpg*
## --> result should contains that: gpg-pubkey-41a40948-4ce19266

# Install the required tools for GitLab
yum -y update
yum -y groupinstall 'Development Tools'
yum -y install vim-enhanced readline readline-devel ncurses-devel gdbm-devel glibc-devel tcl-devel openssl-devel curl-devel expat-devel db4-devel byacc sqlite-devel gcc-c++ libyaml libyaml-devel libffi libffi-devel libxml2 libxml2-devel libxslt libxslt-devel libicu libicu-devel system-config-firewall-tui redis sudo wget crontabs logwatch logrotate perl-Time-HiRes git patch

# Install Python
yum -y install python

# Make sure that Python is 2.5+ (3.x is not supported at the moment)
python --version

# If it's Python 3 you might need to install Python 2 separately
yum -y install python2.7

# Make sure you can access Python via python2
python2 --version

# If you get a "command not found" error create a link to the python binary
ln -s /usr/bin/python /usr/bin/python2

# For reStructuredText markup language support install required package:
yum -y install python-docutils

# Configure redis

## Make sure redis is started on boot:
chkconfig redis on
service redis start

# Install mail server
# In order to receive mail notifications, make sure to install a mail server. The recommended one is postfix and you can install it with:
yum -y install postfix

# Remove the old Ruby 1.8 if present:
yum remove ruby

# Download Ruby and compile it:
mkdir /tmp/ruby && cd /tmp/ruby
curl --progress ftp://ftp.ruby-lang.org/pub/ruby/2.0/ruby-2.0.0-p353.tar.gz | tar xz
cd ruby-2.0.0-p353
./configure --disable-install-rdoc
make
make install

## Install core gems
gem install bundler --no-ri --no-rdoc


# Users

## Create a git user for Gitlab -- For extra security, the shell we use for this user does not allow logins via a terminal.
adduser --system --shell /bin/bash --comment 'GitLab' --create-home --home-dir /home/git/ git
#adduser --system --shell /sbin/nologin --comment 'GitLab' --create-home --home-dir /home/git/ git

## Important: In order to include /usr/local/bin to git user's PATH, one way is to edit the sudoers file.
sed -i "s/ secure_path = \/sbin:\/bin:\/usr\/sbin:\/usr\/bin/ secure_path = \/sbin:\/bin:\/usr\/sbin:\/usr\/bin:\/usr\/local\/bin/g" /etc/sudoers


# GitLab Shell
cd /home/git

## Clone gitlab-shell
sudo -u git -H git clone https://gitlab.com/gitlab-org/gitlab-shell.git -b v1.8.0

cd gitlab-shell

## Edit configuration
sudo -u git -H cp config.yml.example config.yml

# Edit config and replace gitlab_url
# with something like 'http://domain.com/'
sudo -u git -H sed -i "s/gitlab_url: \"http:\/\/localhost\/\"/gitlab_url: \"https:\/\/$GL_HOSTNAME\/\"/g" config.yml

# Do setup
sudo -u git -H /usr/local/bin/ruby ./bin/install

# Edit config and set ca_file and ca_path
sudo -u git -H sed -i "s/#  ca_file: \/etc\/ssl\/cert.pem/   ca_file: \/etc\/nginx\/gitlab.crt/g" config.yml
sudo -u git -H sed -i "s/#  ca_path: \/etc\/pki\/tls\/certs/   ca_path: \/etc\/pki\/tls\/certs/g" config.yml

### Fix wrong mode bits
chmod 600 /home/git/.ssh/authorized_keys
chmod 700 /home/git/.ssh


# Database

## Install PostgreSQL Server
yum -y install postgresql-server postgresql-devel
service postgresql initdb
service postgresql start
chkconfig postgresql on
sudo -u postgres psql -d template1 -c "CREATE USER git WITH PASSWORD '$POSTGRESQL_GIT_PW';"
sudo -u postgres psql -d template1 -c "CREATE DATABASE gitlabhq_production OWNER git;"
sudo service postgresql restart


# GitLab

# We'll install GitLab into home directory of the user "git"
cd /home/git

# Clone GitLab repository
sudo -u git -H git clone https://gitlab.com/gitlab-org/gitlab-ce.git -b 6-6-stable gitlab

cd /home/git/gitlab

# Copy the example GitLab config
sudo -u git -H cp config/gitlab.yml.example config/gitlab.yml

#sudo -u git -H sed -i "s/bin_path: \/usr\/bin\/git/bin_path: \/usr\/local\/bin\/git/g" config/gitlab.yml

# Make sure GitLab can write to the log/ and tmp/ directories
sudo chown -R git log/
sudo chown -R git tmp/
sudo chmod -R u+rwX log/
sudo chmod -R u+rwX tmp/

# Create directory for satellites
sudo -u git -H mkdir /home/git/gitlab-satellites

# Create directories for sockets/pids and make sure GitLab can write to them
sudo -u git -H mkdir tmp/pids/
sudo -u git -H mkdir tmp/sockets/
sudo chmod -R u+rwX tmp/pids/
sudo chmod -R u+rwX tmp/sockets/

# Create public/uploads directory otherwise backup will fail
sudo -u git -H mkdir public/uploads
sudo chmod -R u+rwX public/uploads

# Copy the example Unicorn config
sudo -u git -H cp config/unicorn.rb.example config/unicorn.rb

# Change worker_processes to 3
sudo -u git -H sed -i "s/worker_processes 2/worker_processes 3/g" config/unicorn.rb

### Change gitlabhq hostname to GL_HOSTNAME
sudo -u git -H sed -i "s/ host: localhost/ host: $GL_HOSTNAME/g" config/gitlab.yml

### Change the from email address
sudo -u git -H sed -i "s/email_from: gitlab@localhost/email_from: gitlab@$GL_HOSTNAME/g" config/gitlab.yml
sudo -u git -H sed -i "s/support_email: support@localhost/support_email: support@$GL_HOSTNAME/g" config/gitlab.yml

## Change to HTTPS
sudo -u git -H sed -i "s/port: 80/port: 443/g" config/gitlab.yml
sudo -u git -H sed -i "s/https: false/https: true/g" config/gitlab.yml
sudo -u git -H sed -i "s/# ssl_url:   \"https:\/\/...\"/ssl_url: https:\/\/secure.gravatar.com\/avatar\/\%{hash}?s=\%{size}\&d=mm/g" config/gitlab.yml

# Copy the example Rack attack config
sudo -u git -H cp config/initializers/rack_attack.rb.example config/initializers/rack_attack.rb

# Configure Git global settings for git user, useful when editing via web
# Edit user.email according to what is set in gitlab.yml
sudo -u git -H git config --global user.name "GitLab"
sudo -u git -H git config --global user.email "gitlab@$GL_HOSTNAME"
sudo -u git -H git config --global core.autocrlf input

# For PostgreSQL
sudo -u git -H cp config/database.yml{.postgresql,}
cat > /home/git/gitlab/config/database.pwd << EOF
$POSTGRESQL_GIT_PW
EOF

# Make config/database.yml readable to git only
sudo -u git -H chmod o-rwx config/database.yml

# Install Gems for PostgreSQL (note, the option says "without ... mysql")
cd /home/git/gitlab
sudo -u git -H bundle install --deployment --without development test mysql aws

#Initialize Database and Activate Advanced Features
# Force it to be silent
export force=yes
sudo -u git -H bundle exec rake gitlab:setup RAILS_ENV=production

# Install Init Script
# Download the init script (will be /etc/init.d/gitlab):
wget -O /etc/init.d/gitlab https://gitlab.com/gitlab-org/gitlab-recipes/raw/master/init/sysvinit/centos/gitlab-unicorn
chmod +x /etc/init.d/gitlab
chkconfig --add gitlab

# Make GitLab start on boot:
chkconfig gitlab on

# Set up logrotate
sudo cp lib/support/logrotate/gitlab /etc/logrotate.d/gitlab

# Check if GitLab and its environment are configured correctly:
sudo -u git -H bundle exec rake gitlab:env:info RAILS_ENV=production

# Start your GitLab instance:
service gitlab start

# Compile assets
sudo -u git -H bundle exec rake assets:precompile RAILS_ENV=production

# Nginx
yum -y install nginx
chkconfig nginx on
wget -O /etc/nginx/conf.d/gitlab.conf https://gitlab.com/gitlab-org/gitlab-recipes/raw/master/web-server/nginx/gitlab-ssl

# Edit /etc/nginx/conf.d/gitlab and replace git.example.com with your FQDN. Make sure to read the comments in order to properly set up ssl.
sed -i "s/git.example.com/$GL_HOSTNAME/g" /etc/nginx/conf.d/gitlab.conf

# Add nginx user to git group:
usermod -a -G git nginx
chmod g+rx /home/git/

# generate self-signed cert
cd /etc/nginx/
#sudo openssl req -new -x509 -nodes -days 3560 -out gitlab.crt -keyout gitlab.key
#sudo chmod o-r gitlab.key

# Configure the firewall
## Poke an iptables hole so users can access the web server (http and https ports) and ssh.
lokkit -s http -s https -s ssh

# Restart the service for the changes to take effect:
service iptables save
service iptables restart

# Finally start nginx with:
service nginx start


### Enable and start

service gitlab start

echo "### Done ###############################################"
echo "#"
echo "# You have your PostgreSQL git password in this file:"
echo "# /home/git/gitlab/config/database.pwd"
echo "#"
echo "# Point your browser to:"
echo "# https://$GL_HOSTNAME (or: https://<host-ip>)"
echo "# Default admin username: admin@local.host"
echo "# Default admin password: 5iveL!fe"
echo "#"
echo "###"